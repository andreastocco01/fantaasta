import csv
import pathlib
import os


class Fanta_Allenatore:
    def __init__(self, nome, budget):
        self.nome = nome
        self.budget = int(budget)
        self.giocatori = {}

    def aggiungi_giocatore(self, nome, prezzo):
        isnum = True
        if not prezzo.isdigit():
            isnum = False
        if isnum == True:
            if int(prezzo) <= self.budget:
                self.giocatori[nome] = prezzo
                costo = int(prezzo)
                self.budget = self.budget - costo
                print('(acquisto) giocatore acquistato')
            else:
                print('(acquisto) non hai abbastanza soldi')
        else:
            print('(acquisto) inserire un prezzo valido')

    def stampa(self):
        print(self.nome, self.giocatori, self.budget)

    def aggiungi_giocatore_file(self, nome_prezzo):
        prezzo = ''
        nome = ''
        if ',' not in nome_prezzo:
            ris = nome_prezzo.split(':')
            for i in range(len(ris)):
                if i % 2 == 0:
                    nome = ris[i]
                    nome = nome[2:len(nome)-1]
                else:
                    prezzo = ris[i]
                    prezzo = prezzo[2:len(prezzo)-2]
                if nome != '' and prezzo != '':
                    self.giocatori[nome] = prezzo
        else:
            ris = nome_prezzo.split(',')
            ris_fin = []
            for j in range(len(ris)):
                ris_fin.append(ris[j].split(':'))
            for k in range(len(ris_fin)):
                q = 1
                if k == len(ris_fin) - 1:
                    q = 2
                giocatore = ris_fin[k]
                nome = giocatore[0]
                nome = nome[2:len(nome)-1]
                prezzo = giocatore[1]
                prezzo = prezzo[2:len(prezzo)-q]
                if nome != '' and prezzo != '':
                    self.giocatori[nome] = prezzo


def salvaComandi(comando, riuscito):
    riga = ''
    esito = ''
    path = pathlib.Path(os.getcwd() + '/Squadre.csv')
    if path.is_file():
        f = open(os.getcwd() + '/Cronologia.txt', "a")
        if riuscito == True:
            esito = 'EFFETTUATO'
            riga = comando + '\t' + esito
        else:
            esito = 'NON EFFETTUATO'
            riga = comando + '\t' + esito
        f.write(riga + '\n')
        if comando == 'end':
            f.write('\n\n\n')
    else:
        f = open(os.getcwd() + '/Cronologia.txt', "w")
        if riuscito == True:
            esito = 'EFFETTUATO'
            riga = comando + '\t' + esito
        else:
            esito = 'NON EFFETTUATO'
            riga = comando + '\t' + esito
        f.write(riga + '\n')
        if comando == 'end':
            f.write('\n\n\n')


fanta_allenatori = []

path = pathlib.Path(os.getcwd() + '/Squadre.csv')
if path.is_file():
    with open(os.getcwd() + '/Squadre.csv', 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=';')
        i = 0
        for row in reader:
            if row != [] and row != ['Fanta_Allenatori', 'Giocatori', 'Budget']:
                fanta_allenatori.append(Fanta_Allenatore(row[0], row[2]))
                fanta_allenatori[i].aggiungi_giocatore_file(row[1])
                i += 1
else:
    with open(os.getcwd() + '/Squadre.csv', 'w') as csv_file:
        writer = csv.writer(csv_file, delimiter=';')
        writer.writerow(['Fanta_Allenatori', 'Giocatori', 'Budget'])

conclusione_asta = False
while conclusione_asta == False:
    riuscito = False
    trovata_squadra_eliminare = False
    corrispondenza = False
    trovato_acquirente = False
    corrispondenza_acquisto = False
    comando = input('(Fanta-Calcio): ')
    cmd = comando.split(' ')
    if cmd[0] == 'allenatore':
        corrispondenza = False
        if len(cmd) == 2:
            if cmd[1] == '?':
                print('allenatore <nome_allenatore>')
            else:
                nome_fanta_allenatore = cmd[1]
                nome_fanta_allenatore = nome_fanta_allenatore.lower()
                for i in range(len(fanta_allenatori)):
                    if nome_fanta_allenatore in fanta_allenatori[i].nome:
                        corrispondenza = True
                if corrispondenza == True:
                    print('(allenatore) allenatore già esistente')
                else:
                    fanta_allenatori.append(
                        Fanta_Allenatore(nome_fanta_allenatore, 500))
                    riuscito = True
        else:
            print('(allenatore) comando incompleto')
    elif cmd[0] == 'acquisto':
        corrispondenza = False
        trovato_acquirente = False
        if len(cmd) == 2 and cmd[1] == '?':
            print('acquisto <nome_acquirente> <nome_giocatore> <prezzo>')
        elif len(cmd) == 3 and cmd[2] == '?':
            print('acquisto ' + cmd[1] + ' <nome_giocatore> <prezzo>')
        elif len(cmd) == 4 and cmd[3] == '?':
            print('acquisto ' + cmd[1] + ' ' + cmd[2] + ' <prezzo>')
        elif len(cmd) == 4:
            acquirente = cmd[1]
            acquirente = acquirente.lower()
            for i in range(len(fanta_allenatori)):
                if fanta_allenatori[i].nome == acquirente:
                    trovato_acquirente = True
                    break
            if trovato_acquirente == True:
                nome_giocatore = cmd[2]
                nome_giocatore = nome_giocatore.lower()
                for j in range(len(fanta_allenatori)):
                    if nome_giocatore in fanta_allenatori[j].giocatori:
                        corrispondenza = True
                if corrispondenza == True:
                    print('(acquisto) giocatore già acquistato')
                else:
                    prezzo_giocatore = cmd[3]
                    fanta_allenatori[i].aggiungi_giocatore(
                        nome_giocatore, prezzo_giocatore)
                    riuscito = True
            else:
                print('(acquisto) acquirente non trovato')
        else:
            print('(acquisto) comando incompleto')
    elif cmd[0] == 'status':
        if len(cmd) == 1:
            for i in range(len(fanta_allenatori)):
                print(fanta_allenatori[i].stampa())
            riuscito = True
        else:
            print('(status) comando non valido')
    elif cmd[0] == 'end':
        if len(cmd) == 1:
            conclusione_asta = True
            print('(Fanta-Calcio) asta conclusa')
            with open(os.getcwd() + '/Squadre.csv', 'w') as csv_file:
                writer = csv.writer(csv_file, delimiter=';')
                writer.writerow(['Fanta_Allenatori', 'Giocatori', 'Budget'])
                for i in range(len(fanta_allenatori)):
                    writer.writerow(
                        [fanta_allenatori[i].nome, fanta_allenatori[i].giocatori, fanta_allenatori[i].budget])
                riuscito = True
        else:
            print('(end) comando non valido')

    elif cmd[0] == '?':
        if len(cmd) == 1:
            f = open(os.getcwd() + '/Help.txt', "r")
            print(f.read())
            riuscito = True
        else:
            print('(?) comando non valido')
    elif cmd[0] == 'svincolo':
        if len(cmd) == 2:
            if cmd[1] == '?':
                print('svincolo <nome_giocatore>')
            else:
                nome_giocatore_eliminare = cmd[1]
                nome_giocatore_eliminare = nome_giocatore_eliminare.lower()
                for i in range(len(fanta_allenatori)):
                    if nome_giocatore_eliminare in fanta_allenatori[i].giocatori:
                        print('(svincolo) giocatore svincolato')
                        fanta_allenatori[i].budget = fanta_allenatori[i].budget + \
                            int(fanta_allenatori[i].giocatori[nome_giocatore_eliminare])
                        del fanta_allenatori[i].giocatori[nome_giocatore_eliminare]
                        riuscito = True
                        break
                    else:
                        print('(svincolo) giocatore già svincolato')
        else:
            print('(svincolo) comando incompleto')
    elif cmd[0] == 'elimina_allenatore':
        corrispondenza = False
        if len(cmd) == 2:
            if cmd[1] == '?':
                print('allenatore <nome_allenatore>')
            else:
                nome_allenatore_eliminare = cmd[1]
                nome_allenatore_eliminare = nome_allenatore_eliminare.lower()
                for i in range(len(fanta_allenatori)):
                    if fanta_allenatori[i].nome == nome_allenatore_eliminare:
                        corrispondenza = True
                        break
                if corrispondenza == True:
                    print('(elimina_allenatore) squadra eliminata')
                    del fanta_allenatori[i]
                    riuscito = True
                else:
                    print('(elimina_allenatore) squadra inesistente')
        else:
            print('(elimina_allenatore) comando incompleto')
    elif cmd[0] == 'scambio':
        if len(cmd) > 1:
            if cmd[1] == '?' and len(cmd) == 2:
                print('scambio <nome_giocatore> <nome_giocatore>')
            elif cmd[2] == '?' and len(cmd) == 3:
                print('scambio ' + cmd[1] + ' <nome_giocatore>')
            elif len(cmd) == 3:
                nome_giocatore1 = cmd[1]
                nome_giocatore2 = cmd[2]
                pos1 = -1
                pos2 = -1
                if nome_giocatore1 == nome_giocatore2:
                    print('(scambio) hai inserito lo stesso giocatore')
                else:
                    for Fanta_Allenatore in fanta_allenatori:
                        if nome_giocatore1 in Fanta_Allenatore.giocatori:
                            pos1 = fanta_allenatori.index(Fanta_Allenatore)
                        if nome_giocatore2 in Fanta_Allenatore.giocatori:
                            pos2 = fanta_allenatori.index(Fanta_Allenatore)
                    if pos1 == -1 or pos2 == -1:
                        print('(scambio) giocatori inesistenti')
                    else:

                        fanta_allenatori[pos1].giocatori[nome_giocatore2] = fanta_allenatori[pos2].giocatori[nome_giocatore2]
                        del fanta_allenatori[pos2].giocatori[nome_giocatore2]

                        fanta_allenatori[pos2].giocatori[nome_giocatore1] = fanta_allenatori[pos1].giocatori[nome_giocatore1]
                        del fanta_allenatori[pos1].giocatori[nome_giocatore1]

                        riuscito = True
                        print('(scambio) scambio effettuato')
        else:
            print('(scambio) comando incompleto')
    elif cmd[0] == 'bonus':
        if len(cmd) > 1:
            if cmd[1] == '?' and len(cmd) == 2:
                print('scambio <nome_allenatore> <bonus>')
            elif cmd[2] == '?' and len(cmd) == 3:
                print('scambio ' + cmd[1] + ' <bonus>')
            elif len(cmd) == 3:
                if cmd[2].isdigit():
                    for i in range(len(fanta_allenatori)):
                        if fanta_allenatori[i].nome == cmd[1]:
                            fanta_allenatori[i].budget = fanta_allenatori[i].budget + \
                                int(cmd[2])
                            break
                    if i == len(fanta_allenatori):
                        print('(bonus) allenatore non trovato')
                    else:
                        riuscito = True
                        print('(bonus) transazione eseguita')
                else:
                    print('(bonus) inserire una quantità valida')
        else:
            print('(bonus) comando incompleto')

    else:
        print('(Fanta-Calcio) comando non riconosciuto')
    salvaComandi(comando, riuscito)
